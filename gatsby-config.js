module.exports = {
  siteMetadata: {
    title: `ShouldWeMakeFire`,
    description: `Website to check local weather and backyard fire conditions.`,
    author: `@mathyu05`,
    bitbucket: `https://bitbucket.org/mathyu05/shouldwemakefire`,

  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `ShouldWeMakeFire`,
        short_name: `ShouldWeMakeFire`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#4dc0b5`,
        display: `minimal-ui`,
        icon: `src/images/tailwind-icon.png`
      }
    },
    `gatsby-plugin-postcss`,
    {
      resolve: "gatsby-plugin-purgecss",
      options: {
        tailwind: true,
        purgeOnly: ["src/css/style.css", "src/css/global.css"]
      }
    }
  ]
};
