// See https://tailwindcss.com/docs/configuration for details

module.exports = {
  theme:
  {
    minWidth: {
      '0': '0',
      '1/4': '25%',
      '1/2': '50%',
      '3/4': '75%',
      'full': '100%',
    },
    minHeight: {
      '0': '0',
      '1/4': '25%',
      '1/2': '50%',
      '3/4': '75%',
      'full': '100%',
      'screen': '100vh'
    },
    maxHeight: {
      '0': '0',
      '1/4': '25%',
      '1/2': '50%',
      '3/4': '75%',
      'full': '100%',
      'screen': '100vh'
    },
    extend:
    {
      spacing:
      {
        '72': '18rem',
        '84': '21rem',
        '96': '24rem',
      }
    }
  },
  variants:
  {
  },
  plugins: [
  ]
};
